import numpy
import pandas as pd
mybaccs = []    #Empty list of accession numbers
r = pd.read_csv("mybaccs.csv")  #read in csv file of accession numbers
mybaccs = r["mybacc"] #store accession numbers in mybaccs
#reformatting mybaccs
z = mybaccs.str.split(".", expand=True)
mybaccs = z.drop(z.columns[[1]], axis=1)
mybaccs = z[z.columns[0]].tolist()

#print(z)
print(mybaccs)

df = pd.read_csv("uniquebaccs.csv")  #reading accession numbers with specific gene ids
#print(x)


idlist = list(df['mybacc'])  # Storing the previous in idlist
#print(idlist)
df = df.fillna('')

fh = open("mybacc.geneid.csv","w")
print(len(mybaccs))
# Looping through mybaccs and idllist to get the specific gene id for mybaccs and write out to file
for id in mybaccs:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    target = df.iloc[index][1]
    #print(id)
    #if numpy.isnan(target) : target = ""
    #print(target)
    fh.write(str(id)+","+target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()