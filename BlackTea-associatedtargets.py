import numpy
import pandas as pd
mybaids = []    #Creating empty black tea aid list
r = pd.read_csv("mybaids.csv")   #Read in aids csv file
mybaids = list(r["mybaid"])  #Store values into the mybaids list


df = pd.read_csv("uniquebaids.csv")   #Read in csv of unique cids and aids



idlist = list(df['mybaid'])    #Store unique values
#print(idlist)
df = df.fillna('')

fh = open("mybaids.target.csv","w")
print(len(mybaids))
# Looping through mybaids to determine their specific cid values and writing it to a new file
for id in mybaids:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    target = df.iloc[index][1]
    #print(id)
    #if numpy.isnan(target) : target = ""
    #print(target)
    fh.write(str(id)+","+target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()
