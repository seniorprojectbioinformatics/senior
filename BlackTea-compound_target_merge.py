import pandas as pd
mycomps = []   #Empty list of compounds
r = pd.read_csv("mycomps.csv")  #Read in csv of compounds
mycomps = list(r["mycomp"])   #Store these values in mycomps
#print(myaids)

df = pd.read_csv("uniquecomps.csv")   # Read in csv of of unique compounds as their cids
#print(x)


idlist = list(df['mycomp'])  # Store values into idlist
#print(idlist)
df = df.fillna('')

fh = open("comp.target.csv","w")
#print(len(mycomps))
# looping through the list of mycomps to determine their specific targets and print them out
for id in mycomps:
  #print(id)
  if(id in idlist):
    index = idlist.index(id)
    print(index)
    target = df.iloc[index][1]
    print(id)
    #if numpy.isnan(target) : target = ""
    print(target)
    fh.write(str(id)+"," + target+"\n")
  else:
    fh.write(str(id)+"\n")
fh.close()
